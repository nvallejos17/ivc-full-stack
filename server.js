const express = require('express');
const connectDB = require('./config/db');
const cors = require('cors');

const app = express();

app.use(cors());

// Connect DB
connectDB();

// Static assets
app.use(express.static(`${__dirname}/client/build`));

// Add body-parser middleware included in Express
app.use(express.json());

// Routes
app.use('/api/clients', require('./routes/api/clients'));
app.use('/api/reports', require('./routes/api/reports'));
app.use('/api/users', require('./routes/api/users'));

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});
