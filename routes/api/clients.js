const express = require('express');
const router = express.Router();
const path = require('path');
const auth = require('../../middleware/auth');
const { check, validationResult } = require('express-validator');

const Client = require('../../models/Client');

// Multer
const multer = require('multer');
const storage = multer.diskStorage({
  destination: 'client/build/logos/',
  filename: (req, file, cb) => {
    cb(
      null,
      file.fieldname + '-' + Date.now() + path.extname(file.originalname)
    );
  },
});

const upload = multer({ storage: storage });

// @route   PUT api/clients/upload
// @desc    Upload logo
// @access  Private
router.put('/upload', [auth, upload.single('logoFile')], async (req, res) => {
  const { clientId } = req.body;

  try {
    console.log(req.file);
    console.log(clientId);

    // Find client by id
    let client = await Client.findById(clientId);

    if (!client)
      return res.status(404).json({ errors: [{ msg: 'Client not found' }] });

    // Update client logoUrl
    const updatedClient = { logoUrl: req.file.filename };

    client = await Client.findByIdAndUpdate(
      clientId,
      {
        $set: updatedClient,
      },
      { new: true }
    );

    // Return updated client
    res.json(client);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server error');
  }
});

// @route   GET api/clients
// @desc    Get all clients
// @access  Private
router.get('/', auth, async (req, res) => {
  try {
    const clients = await Client.find({}).sort({ name: 1 });

    res.json(clients);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server error');
  }
});

// @route   POST api/clients
// @desc    Add new client
// @access  Private
router.post(
  '/',
  [
    auth,
    [
      check('name', 'Name is required').not().isEmpty(),
      check('url', 'Please, include a valid URL').isURL(),
      check('email', 'Please, include a valid email').isEmail(),
      check('phone', 'Please, include a valid phone number').isNumeric(),
      check('apiKey', 'Google Analytics ID is required').not().isEmpty(),
    ],
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const {
      // Required
      name,
      url,
      email,
      phone,
      apiKey,

      // Misc
      editor,
      address,
      city,
      classification,
      subClassification,
      sections,
    } = req.body;

    try {
      // Check if client already exists
      let client = await Client.findOne({ email });
      if (client) {
        return res
          .status(400)
          .json({ errors: [{ msg: 'Client already exists' }] });
      }

      // Create new client
      client = new Client({
        // Created by (added from auth middleware)
        userId: req.user.id,

        // Required
        name,
        url,
        email,
        phone,
        apiKey,

        // Misc
        editor: '',
        address: '',
        city: '',
        classification: '',
        subClassification: '',
        logoUrl: '',
        sections: '',
      });

      if (editor) client.editor = editor;
      if (address) client.address = address;
      if (city) client.city = city;
      if (classification) client.classification = classification;
      if (subClassification) client.subClassification = subClassification;
      if (sections) client.sections = sections;

      // Save in DB (this also adds an id field)
      await client.save();

      res.json(client);
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server error');
    }
  }
);

// @route   PUT api/clients/:id
// @desc    Edit client by ID
// @access  Private
router.put('/:id', auth, async (req, res) => {
  const {
    // Required
    name,
    url,
    email,
    phone,
    apiKey,

    // Misc
    editor,
    address,
    city,
    classification,
    subClassification,
    sections,
  } = req.body;

  // Build object for updated values
  const updatedClient = {};
  if (name) updatedClient.name = name;
  if (url) updatedClient.url = url;
  if (email) updatedClient.email = email;
  if (phone) updatedClient.phone = phone;
  if (apiKey) updatedClient.apiKey = apiKey;
  if (editor) updatedClient.editor = editor;
  if (address) updatedClient.address = address;
  if (city) updatedClient.city = city;
  if (classification) updatedClient.classification = classification;
  if (subClassification) updatedClient.subClassification = subClassification;
  if (sections) updatedClient.sections = sections;

  try {
    let client = await Client.findById(req.params.id);

    if (!client)
      return res.status(404).json({ errors: [{ msg: 'Client not found' }] });

    client = await Client.findByIdAndUpdate(
      req.params.id,
      {
        $set: updatedClient,
      },
      { new: true }
    );

    res.json(client);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server error');
  }
});

module.exports = router;
