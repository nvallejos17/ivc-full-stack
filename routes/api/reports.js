const express = require('express');
const fs = require('fs');
const router = express.Router();
const auth = require('../../middleware/auth');

const Report = require('../../models/Report');

// @route   GET api/reports/download/:filename
// @desc    Download report by filename
// @access  Public
router.get('/download/:filename', async (req, res) => {
  const document = `/usr/src/app/recurso/${req.params.filename}`;
  fs.readFile(document, (err, data) => {
    res.contentType('application/pdf');
    res.send(data);
  });
});

// @route   GET api/reports/:clientId
// @desc    Get reports by client's ID
// @access  Private
router.get('/:clientId', auth, async (req, res) => {
  try {
    const reports = await Report.find({ clientId: req.params.clientId }).sort({
      date: -1,
    });

    res.json(reports);
  } catch (error) {
    console.error(err.message);
    res.status(500).send('Server error');
  }
});

// @route   POST api/reports
// @desc    Generate new report
// @access  Private
router.post('/', auth, async (req, res) => {
  const { clientId, startDate, endDate, filename } = req.body;

  try {
    let report = new Report({
      userId: req.user.id,
      clientId,
      startDate,
      endDate,
      filename,
    });

    await report.save();

    res.json(report);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server error');
  }
});

// @route   DELETE api/reports/:userId/:filename
// @desc    Delete a report by filename
// @access  Private

router.delete('/:filename', auth, async (req, res) => {
  try {
    const document = `/usr/src/app/recurso/${req.params.filename}`;
    Report.findOneAndDelete({
      filename: req.params.filename
      })
      .exec((err, post) => {
        if(err)
          return res.status(500).json({code: 500, message: 'There was an error deleting the report', error: err})
        res.status(200).json({code: 200, message: 'Report deleted', deletedPost: post})
    });
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server error');
  }
  try {
    fs.unlink(document)
  } catch (err) {
    console.error(err.message);
  }
});

module.exports = router;
