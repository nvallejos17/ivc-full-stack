import React, { useState } from 'react';

// Connect to Redux
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

// Redux actions
import { addClient } from '../../actions/clients';

const AddClientModal = ({ addClient }) => {
  const [formData, setFormData] = useState({
    name: '',
    url: '',
    editor: '',
    address: '',
    city: '',
    phone: '',
    // fax: '',
    email: '',
    classification: '',
    subClassification: '',
    apiKey: '',
    sections: '',
  });

  const {
    name,
    url,
    editor,
    address,
    city,
    phone,
    // fax,
    email,
    classification,
    subClassification,
    apiKey,
    sections,
  } = formData;

  const onChange = (e) =>
    // Copy the whole object and update target
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = async (e) => {
    e.preventDefault();

    addClient(formData);

    setFormData({
      name: '',
      url: '',
      editor: '',
      address: '',
      city: '',
      phone: '',
      // fax: '',
      email: '',
      classification: '',
      subClassification: '',
      apiKey: '',
      sections: '',
    });
  };

  return (
    <div
      className='modal fade'
      id='addClientModal'
      tabIndex='-1'
      role='dialog'
      aria-labelledby='addClientLabel'
      aria-hidden='true'
    >
      <div className='modal-dialog' role='document'>
        <div className='modal-content'>
          <div className='modal-header'>
            <h5 className='modal-title' id='addClientLabel'>
              New client
            </h5>
            <button
              type='button'
              className='close'
              data-dismiss='modal'
              aria-label='Close'
            >
              <span aria-hidden='true'>&times;</span>
            </button>
          </div>
          <div className='modal-body'>
            <form className='d-flex flex-column' onSubmit={(e) => onSubmit(e)}>
              <div className='form-group'>
                <input
                  type='text'
                  className='form-control'
                  placeholder='Name'
                  name='name'
                  value={name}
                  onChange={onChange}
                  required
                />
              </div>

              <div className='form-group'>
                <input
                  type='url'
                  className='form-control'
                  placeholder='URL'
                  name='url'
                  value={url}
                  onChange={onChange}
                  required
                />
              </div>

              <div className='form-group'>
                <input
                  type='text'
                  className='form-control'
                  placeholder='Editor'
                  name='editor'
                  value={editor}
                  onChange={onChange}
                />
              </div>

              <div className='form-group'>
                <input
                  type='text'
                  className='form-control'
                  placeholder='Address'
                  name='address'
                  value={address}
                  onChange={onChange}
                />
              </div>

              <div className='form-group'>
                <input
                  type='text'
                  className='form-control'
                  placeholder='City'
                  name='city'
                  value={city}
                  onChange={onChange}
                />
              </div>

              <div className='form-group'>
                <input
                  type='text'
                  className='form-control'
                  placeholder='Phone'
                  name='phone'
                  value={phone}
                  onChange={onChange}
                  required
                />
              </div>

              <div className='form-group'>
                <input
                  type='email'
                  className='form-control'
                  placeholder='Email'
                  name='email'
                  value={email}
                  onChange={onChange}
                  required
                />
              </div>

              <div className='form-row'>
                <div className='form-group col-6'>
                  <input
                    type='text'
                    className='form-control'
                    placeholder='Classification'
                    name='classification'
                    value={classification}
                    onChange={onChange}
                  />
                </div>

                <div className='form-group col-6'>
                  <input
                    type='text'
                    className='form-control'
                    placeholder='Sub-classification'
                    name='subClassification'
                    value={subClassification}
                    onChange={onChange}
                  />
                </div>
              </div>

              <div className='form-group'>
                <input
                  type='text'
                  className='form-control'
                  placeholder='Google Analytics ID'
                  name='apiKey'
                  value={apiKey}
                  onChange={onChange}
                  required
                />
              </div>
              <div className='form-group'>
                <input
                  type='text'
                  className='form-control'
                  placeholder='Sections'
                  name='sections'
                  value={sections}
                  onChange={onChange}
                />
              </div>


              <button
                type='submit'
                className='btn btn-secondary align-self-end'
                data-toggle='modal'
                data-target='#addClientModal'
              >
                Add client
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

AddClientModal.propTypes = {
  addClient: PropTypes.func.isRequired,
};

export default connect(null, { addClient })(AddClientModal);
