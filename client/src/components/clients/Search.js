import React, { useRef, useEffect } from 'react';

// Connect to Redux
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

// Redux actions
import { filterClients, clearFilter } from '../../actions/clients';

const Search = ({ filtered, filterClients, clearFilter }) => {
  const text = useRef('');

  useEffect(() => {
    if (filtered === null) {
      text.current.value = '';
    }
  });

  const onChange = (e) => {
    if (text.current.value !== '') {
      filterClients(e.target.value);
    } else {
      clearFilter();
    }
  };

  return (
    <div className='input-group mb-3'>
      <input
        ref={text}
        type='text'
        className='form-control'
        placeholder='Search client'
        onChange={(e) => onChange(e)}
      />
    </div>
  );
};

Search.propTypes = {
  filtered: PropTypes.array,
  filterClients: PropTypes.func.isRequired,
  clearFilter: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  filtered: state.clients.filtered,
});

export default connect(mapStateToProps, { filterClients, clearFilter })(Search);
