import React from 'react';

// Connect to Redux
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

// Redux actions
import { selectClient } from '../../actions/clients';
import { getReports } from '../../actions/reports';

const ClientItem = ({ client, selectClient, getReports }) => {
  const onClick = async () => {
    selectClient(client);
    getReports(client);
  };

  return (
    <div className='card mb-3'>
      <div className='card-body d-flex justify-content-between align-items-center'>
        <h5 className='card-title m-0'>{client.name}</h5>
        <button className='btn btn-sm btn-secondary' onClick={onClick}>
          <i className='fas fa-eye'></i>
        </button>
      </div>
    </div>
  );
};

ClientItem.propTypes = {
  selectClient: PropTypes.func.isRequired,
  getReports: PropTypes.func.isRequired,
};

export default connect(null, { selectClient, getReports })(ClientItem);
