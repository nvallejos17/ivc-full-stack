import React, { useState } from 'react';

// Connect to Redux
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

// Redux actions
import { uploadLogo } from '../../actions/clients';

const UploadLogo = ({ selectedClient, uploadLogo }) => {
  const [selectedFile, setFile] = useState(null);

  const onFileChange = (e) => setFile(e.target.files[0]);

  const onSubmit = (e) => {
    e.preventDefault();

    const formData = new FormData();
    formData.append('logoFile', selectedFile);
    formData.append('clientId', selectedClient._id);

    uploadLogo(formData);

    setFile(null);
  };

  return (
    <div
      className='modal fade'
      id='uploadLogoModal'
      tabIndex='-1'
      role='dialog'
      aria-labelledby='uploadLogoLabel'
      aria-hidden='true'
      data-backdrop='static'
    >
      <div className='modal-dialog' role='document'>
        <div className='modal-content'>
          <div className='modal-header'>
            <h5 className='modal-title' id='uploadLogoLabel'>
              Upload logo
            </h5>
            <button
              type='button'
              className='close'
              data-dismiss='modal'
              aria-label='Close'
            >
              <span aria-hidden='true'>&times;</span>
            </button>
          </div>
          <div className='modal-body'>
            <form className='d-flex flex-column' onSubmit={onSubmit}>
              <div className='input-group mb-3'>
                <div className='custom-file'>
                  <input
                    type='file'
                    id='logoFile'
                    name='logoFile'
                    className='custom-file-input'
                    onChange={onFileChange}
                  />
                  <label className='custom-file-label' htmlFor='logo'>
                    {selectedFile ? selectedFile.name : 'Choose file'}
                  </label>
                </div>
              </div>

              <button
                type='submit'
                className='btn btn-secondary align-self-end'
                data-toggle='modal'
                data-target='#uploadLogoModal'
              >
                Upload
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

UploadLogo.propTypes = {
  selectedClient: PropTypes.object,
  uploadLogo: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  selectedClient: state.clients.selectedClient,
});

export default connect(mapStateToProps, { uploadLogo })(UploadLogo);
