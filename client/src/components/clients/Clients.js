import React, { useEffect, Fragment } from 'react';

import ClientItem from './ClientItem';
import Preloader from '../layout/Preloader';

// Connect to Redux
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

// Redux actions
import { getClients } from '../../actions/clients';

const Clients = ({ clients: { clients, loading, filtered }, getClients }) => {
  useEffect(() => {
    getClients();
    // eslint-disable-next-line
  }, []);

  if (loading || clients === null) return <Preloader />;

  if (!loading && clients.length === 0)
    return <h5>Sorry, there are no clients.</h5>;

  return (
    <Fragment>
      {filtered !== null
        ? filtered.map((client) => (
            <ClientItem client={client} key={client._id} />
          ))
        : clients.map((client) => (
            <ClientItem client={client} key={client._id} />
          ))}
    </Fragment>
  );
};

// (add these to main function parameters)
Clients.propTypes = {
  clients: PropTypes.object.isRequired,
  getClients: PropTypes.func,
};

// Convert state to props (add these to propTypes and main function parameters)
const mapStateToProps = (state) => ({
  clients: state.clients,
});

export default connect(mapStateToProps, { getClients })(Clients);
