import React, { Fragment } from 'react';

import AddReportModal from '../reports/AddReportModal';
import EditClientModal from '../clients/EditClientModal';
import UploadLogo from './UploadLogo';

// Connect to Redux
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

const ClientInfo = ({ selectedClient }) => {
  return (
    <Fragment>
      {!selectedClient ? (
        <h5 className='mb-0'>Please, select a client.</h5>
      ) : (
        <div className='d-flex align-items-center p-3'>
          {/* Left content */}
          <div className='d-flex flex-column  pr-3'>
            {selectedClient.logoUrl !== '' ? (
              <img
                src={`https://certificados.ivc.org.ar/logos/${selectedClient.logoUrl}`}
                alt={selectedClient.logoUrl}
                className='mb-3'
                style={{ width: '120px' }}
              />
            ) : (
              <div
                className='logo bg-light mb-3'
                style={{ width: '120px', height: '120px', borderRadius: '50%' }}
              ></div>
            )}

            <div className='btn-group'>
              <button
                type='button'
                className='btn btn-secondary'
                data-toggle='modal'
                data-target='#editClientModal'
              >
                <i className='fas fa-edit'></i>
              </button>

              <button
                type='button'
                className='btn btn-success'
                data-toggle='modal'
                data-target='#addReportModal'
              >
                <i className='fas fa-file-medical'></i>
              </button>

              <button
                type='button'
                className='btn btn-secondary'
                data-toggle='modal'
                data-target='#uploadLogoModal'
              >
                <i className='fas fa-camera'></i>
              </button>
            </div>
            <EditClientModal />
            <AddReportModal />
            <UploadLogo />
          </div>

          {/* Right content */}
          <div className='row no-gutters w-100 p-0'>
            {/* Column 2.1 */}
            <div className='col-4'>
              <ul className='list-group list-group-flush pl-3'>
                <li className='list-group-item p-1'>
                  Name:{' '}
                  <span className='text-muted'>{selectedClient.name}</span>
                </li>
                <li className='list-group-item p-1'>
                  URL: <span className='text-muted'>{selectedClient.url}</span>
                </li>
                <li className='list-group-item p-1'>
                  Editor:{' '}
                  <span className='text-muted'>{selectedClient.editor}</span>
                </li>
              </ul>
            </div>

            {/* Column 2.2 */}
            <div className='col-4'>
              <ul className='list-group list-group-flush pl-3'>
                <li className='list-group-item p-1'>
                  Address:{' '}
                  <span className='text-muted'>{selectedClient.address}</span>
                </li>
                <li className='list-group-item p-1'>
                  City:{' '}
                  <span className='text-muted'>{selectedClient.city}</span>
                </li>
                <li className='list-group-item p-1'>
                  Phone:{' '}
                  <span className='text-muted'>{selectedClient.phone}</span>
                </li>
                {/* <li className='list-group-item p-1'>
                  Fax: <span className='text-muted'>{selectedClient.fax}</span>
                </li> */}
                <li className='list-group-item p-1'>
                  Email:{' '}
                  <span className='text-muted'>{selectedClient.email}</span>
                </li>
              </ul>
            </div>

            {/* Column 2.3 */}
            <div className='col-4'>
              <ul className='list-group list-group-flush pl-3'>
                <li className='list-group-item p-1'>
                  Classification:{' '}
                  <span className='text-muted'>
                    {selectedClient.classification}
                  </span>
                </li>
                <li className='list-group-item p-1'>
                  Sub-classification:{' '}
                  <span className='text-muted'>
                    {selectedClient.subClassification}
                  </span>
                </li>
                <li className='list-group-item p-1'>
                  Google Analytics ID:{' '}
                  <span className='text-muted'>{selectedClient.apiKey}</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      )}
    </Fragment>
  );
};

ClientInfo.propTypes = {
  selectedClient: PropTypes.object,
};

const mapStateToProps = (state) => ({
  selectedClient: state.clients.selectedClient,
});

export default connect(mapStateToProps)(ClientInfo);
