import React from 'react';

import Clients from '../clients/Clients';
import ClientInfo from '../clients/ClientInfo';
import Search from '../clients/Search';
import Reports from '../reports/Reports';

import AddClientModal from '../clients/AddClientModal';

const Dashboard = () => {
  return (
    <div className='container-fluid row no-gutters h-100 p-0'>
      {/* Left side */}
      <div className='col-3 d-flex flex-column align-items-center h-100 p-3'>
        <Search />

        <div className='w-100 h-100 mb-3 p-3 border rounded overflow-auto'>
          <Clients />
        </div>

        <button
          type='button'
          className='btn btn-secondary'
          data-toggle='modal'
          data-target='#addClientModal'
        >
          <i className='fas fa-user-plus'></i> Add client
        </button>
        <AddClientModal />
      </div>

      {/* Right side */}
      <div className='col-9 d-flex flex-column h-100 py-3 pl-0 pr-3'>
        {/* Client info */}
        <div className='w-100 mb-3 p-3 border rounded'>
          <ClientInfo />
        </div>

        {/* Reports */}
        <div className='w-100 h-100 p-3 border rounded overflow-auto'>
          <Reports />
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
