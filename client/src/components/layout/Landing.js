import React from 'react';
import landingBrand from './img/landing-brand.png';

const style = {
  maxWidth: '320px',
  width: '100%',
};

const Landing = () => {
  return (
    <div className='container-fluid bg-light d-flex justify-content-center align-items-center h-100'>
      <img src={landingBrand} alt='' style={style} />
    </div>
  );
};

export default Landing;
