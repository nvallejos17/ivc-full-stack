import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import navBrand from './img/nav-brand.png';

// Connect to Redux
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

// Redux actions
import { logout } from '../../actions/auth';

const Navbar = ({ logout, auth: { isAuthenticated, loading } }) => {
  const authLinks = (
    <ul className='navbar-nav'>
      <li className='nav-item'>
        <Link to='/dashboard' className='nav-link'>
          Dashboard
        </Link>
      </li>
      <li className='nav-item'>
        <Link to='/login' className='nav-link pr-0' onClick={logout}>
          <i className='fas fa-sign-out-alt'></i> Logout
        </Link>
      </li>
    </ul>
  );

  const guestLinks = (
    <ul className='navbar-nav'>
      <li className='nav-item'>
        <Link to='/login' className='nav-link'>
          Login
        </Link>
      </li>
      {/* <li className='nav-item'>
        <Link to='/register' className='nav-link pr-0'>
          Register
        </Link>
      </li> */}
    </ul>
  );

  return (
    <nav className='navbar navbar-expand-lg navbar-dark bg-dark fixed-top'>
      <Link to='/' className='navbar-brand'>
        <img
          src={navBrand}
          alt=''
          style={{ height: '48px', transform: 'translateY(-12px)' }}
        />
        {/* IVC Reports */}
      </Link>

      {/* Toggler icon */}
      <button
        className='navbar-toggler'
        type='button'
        data-toggle='collapse'
        data-target='#navbarNav'
        aria-controls='navbarNav'
        aria-expanded='false'
        aria-label='Toggle navigation'
      >
        <span className='navbar-toggler-icon'></span>
      </button>

      {/* Navigation */}
      <div
        className='collapse navbar-collapse justify-content-end'
        id='navbarNav'
      >
        {!loading && (
          <Fragment>{isAuthenticated ? authLinks : guestLinks}</Fragment>
        )}
      </div>
    </nav>
  );
};

Navbar.propTypes = {
  logout: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps, { logout })(Navbar);
