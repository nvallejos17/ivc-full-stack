import React from 'react';

// Connect to Redux
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

const style = {
  position: 'absolute',
  top: '90px',
  left: '1rem',
  zIndex: '1050',
};

const Alerts = ({ alerts }) => {
  return (
    alerts !== null &&
    alerts.length > 0 && (
      <div style={style}>
        {alerts.map((alert) => (
          <div
            key={alert.id}
            className={`alert alert-${alert.alertType}`}
            role='alert'
          >
            {alert.msg}
          </div>
        ))}
      </div>
    )
  );
};

Alerts.propTypes = {
  alerts: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
  alerts: state.alerts,
});

export default connect(mapStateToProps)(Alerts);
