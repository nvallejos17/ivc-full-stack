import React from 'react';

// Connect to Redux
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

const ReportPreloader = ({ generating }) => {
  return (
    generating && (
      <div
        className='fixed-top w-100 h-100 d-flex justify-content-center align-items-center'
        style={{
          zIndex: '1050',
          background: 'rgba(0, 0, 0, 0.4)',
        }}
      >
        <div
          className='border rounded-circle d-flex justify-content-center align-items-center bg-light'
          style={{ padding: '1rem' }}
        >
          <div
            className='spinner-grow text-danger'
            style={{ width: '4rem', height: '4rem' }}
            role='status'
          >
            <span className='sr-only'>Loading...</span>
          </div>
        </div>
      </div>
    )
  );
};

ReportPreloader.propTypes = {
  generating: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  generating: state.reports.generating,
});

export default connect(mapStateToProps)(ReportPreloader);
