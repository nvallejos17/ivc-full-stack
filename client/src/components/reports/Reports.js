import React from 'react';

import ReportItem from './ReportItem';
import Preloader from '../layout/Preloader';

// Connect to Redux
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

const Reports = ({ selectedClient, reports: { reports, loading } }) => {
  if (selectedClient === null)
    return <h5>Client's reports will be displayed here.</h5>;

  if (loading || reports === null) return <Preloader />;

  return !loading && reports.length === 0 ? (
    <h5>Sorry, there are no reports for this client.</h5>
  ) : (
    <table className='table table-striped text-center'>
      <thead className='thead-dark'>
        <tr>
          <th>Report No.</th>
          <th>From</th>
          <th>To</th>
          <th>Created by</th>
          <th>Options</th>
        </tr>
      </thead>

      <tbody>
        {reports.map((report) => (
          <ReportItem report={report} key={report._id} />
        ))}
      </tbody>
    </table>
  );
};

Reports.propTypes = {
  selectedClient: PropTypes.object,
  reports: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  selectedClient: state.clients.selectedClient,
  reports: state.reports,
});

export default connect(mapStateToProps)(Reports);
