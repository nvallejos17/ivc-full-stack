import React, { useState } from 'react';
import moment from 'moment';
import axios from 'axios';

// Connect to Redux
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

// Redux actions
import { setAlert } from '../../actions/alerts';
import { addReport, showPreloader, hidePreloader } from '../../actions/reports';

const AddReportModal = ({
  setAlert,
  selectedClient,
  addReport,
  showPreloader,
  hidePreloader,
}) => {
  const [formData, setFormData] = useState({
    startDate: moment(new Date()).subtract(1, 'month').format('YYYY[-]MM[-]DD'),
    endDate: moment(new Date()).format('YYYY[-]MM[-]DD'),
  });

  const { startDate, endDate } = formData;

  const onChange = (e) =>
    // Copy the whole object and update target
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });

  const dateRangeValidation = () => {
    if (startDate > endDate) {
      setAlert('START DATE must be lower than END DATE', 'danger');
      return false;
    } else if (endDate > moment(new Date()).format('YYYY[-]MM[-]DD')) {
      setAlert('END DATE must be lower than TODAY', 'danger');
      return false;
    }
    return true;
  };

  const onSubmit = async (e) => {
    e.preventDefault();

    if (dateRangeValidation()) {
      // Prepare values to generate report
      const input = {
        dateRange: {
          startDate,
          endDate,
        },
        company_data: selectedClient,
      };

      try {
        // Show report preloader
        showPreloader();

        // Generate report and get filename
        const res = await axios.post(
          'https://generator.ivc.org.ar:8000',
          input
        );

        // Prepare report values to add
        const report = {
          clientId: selectedClient._id,
          startDate,
          endDate,
          filename: res.data,
        };

        // Add report
        addReport(report);
      } catch (err) {
        // Console error
        console.error(err);

        // Show message in frontend
        setAlert("Please, verify your client's Google Analytics ID", 'danger');
      }
    }

    // Hide report preloader
    hidePreloader();

    // Clear form
    setFormData({
      startDate: moment(new Date())
        .subtract(1, 'month')
        .format('YYYY[-]MM[-]DD'),
      endDate: moment(new Date()).format('YYYY[-]MM[-]DD'),
    });
  };

  return (
    <div
      className='modal fade'
      id='addReportModal'
      tabIndex='-1'
      role='dialog'
      aria-labelledby='addReportLabel'
      aria-hidden='true'
      data-backdrop='static'
    >
      <div className='modal-dialog' role='document'>
        <div className='modal-content'>
          <div className='modal-header'>
            <h5 className='modal-title' id='addReportLabel'>
              New report
            </h5>
            <button
              type='button'
              className='close'
              data-dismiss='modal'
              aria-label='Close'
            >
              <span aria-hidden='true'>&times;</span>
            </button>
          </div>
          <div className='modal-body'>
            <form className='d-flex flex-column' onSubmit={(e) => onSubmit(e)}>
              <div className='form-group'>
                <label htmlFor='startDate'>Start date</label>
                <input
                  type='date'
                  className='form-control'
                  id='from'
                  placeholder='dd/mm/aaaa'
                  name='startDate'
                  value={startDate}
                  onChange={onChange}
                  required
                />
              </div>

              <div className='form-group'>
                <label htmlFor='endDate'>End date</label>
                <input
                  type='date'
                  className='form-control'
                  id='to'
                  placeholder='dd/mm/aaaa'
                  name='endDate'
                  value={endDate}
                  onChange={onChange}
                  required
                />
              </div>

              <button
                type='submit'
                className='btn btn-secondary align-self-end'
                data-toggle='modal'
                data-target='#addReportModal'
              >
                Generate report
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

AddReportModal.propTypes = {
  setAlert: PropTypes.func.isRequired,
  selectedClient: PropTypes.object.isRequired,
  addReport: PropTypes.func.isRequired,
  showPreloader: PropTypes.func.isRequired,
  hidePreloader: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  selectedClient: state.clients.selectedClient,
});

export default connect(mapStateToProps, {
  setAlert,
  addReport,
  showPreloader,
  hidePreloader,
})(AddReportModal);
