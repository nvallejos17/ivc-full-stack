import React, { useState } from 'react';

import moment from 'moment';
import axios from 'axios';
import { connect } from 'react-redux';
import { deleteReport } from '../../actions/reports'

const ReportItem = ({ report, deleteReport }) => {
  const { userId, startDate, endDate, filename } = report;

  // Get owners name
  const [createdBy, setOwner] = useState('');
  axios.get(`/api/users/${userId}`).then((res) => setOwner(res.data.name));

  const onClick = (e) => {
    e.preventDefault();

    axios(`/api/reports/download/${filename}`, {
      method: 'GET',
      responseType: 'blob',
    })
      .then((res) => {
        const file = new Blob([res.data], { type: 'application/pdf' });
        const fileURL = URL.createObjectURL(file);
        window.open(fileURL);
      })
      .catch((err) => console.log(err));
  };

  const onClickDelete = (e) => {
    e.preventDefault();

    deleteReport(report);

  };

  return (
    <tr>
      <td className='align-middle'>
        {filename.substring(0, filename.length - 21)}
      </td>
      <td className='align-middle'>
        {moment(startDate).format('DD[/]MM[/]YYYY')}
      </td>
      <td className='align-middle'>
        {moment(endDate).format('DD[/]MM[/]YYYY')}
      </td>
      <td className='align-middle'>{createdBy}</td>
      <td className='align-middle'>
        <div className="btn-group">
          <button
            className='btn btn-sm btn-success'
            target='_blank'
            onClick={onClick}
          >
            <i class="fas fa-download"></i>
          </button>
          <button
            className='btn btn-sm btn-danger'
            target='_blank'
            onClick={onClickDelete}
          >
            <i className='fas fa-trash-alt'></i>
          </button>
        </div>
      </td>
    </tr>
  );
};

export default connect(null, { deleteReport })(ReportItem);
