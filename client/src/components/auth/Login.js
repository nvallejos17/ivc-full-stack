import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';

// Connect to Redux
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

// Redux actions
import { login } from '../../actions/auth';

const style = {
  padding: '1rem',
  paddingTop: '4rem',
};

const Login = ({ login, isAuthenticated }) => {
  const [formData, setFormData] = useState({
    email: '',
    password: '',
  });

  const { email, password } = formData;

  const onChange = (e) =>
    // Copy the whole object and update target
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = async (e) => {
    e.preventDefault();

    login({ email, password });
  };

  if (isAuthenticated) {
    return <Redirect to='/dashboard' />;
  }

  return (
    <div
      className='d-flex flex-column align-items-center w-100 h-100'
      style={style}
    >
      <h3>Login</h3>
      <form
        className='p-3'
        style={{ width: '360px' }}
        onSubmit={(e) => onSubmit(e)}
      >
        <div className='form-group'>
          <input
            type='email'
            className='form-control'
            placeholder='Email'
            name='email'
            value={email}
            onChange={(e) => onChange(e)}
            required
          />
        </div>

        <div className='form-group'>
          <input
            type='password'
            className='form-control'
            placeholder='Password'
            name='password'
            value={password}
            onChange={(e) => onChange(e)}
            minLength='6'
          />
        </div>

        <button type='submit' className='btn btn-secondary w-100'>
          Submit
        </button>
      </form>
    </div>
  );
};

// (add these to main function parameters)
Login.propTypes = {
  login: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool,
};

// Convert state to props (add these to propTypes and main function parameters)
const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps, { login })(Login);
