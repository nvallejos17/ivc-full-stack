import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';

// Connect to Redux
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

// Redux actions
import { register } from '../../actions/auth';
import { setAlert } from '../../actions/alerts';

const style = {
  padding: '1rem',
  paddingTop: '4rem',
};

const Register = ({ register, isAuthenticated, setAlert }) => {
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    password: '',
    password2: '',
  });

  const { name, email, password, password2 } = formData;

  const onChange = (e) =>
    // Copy the whole object and update target
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = async (e) => {
    e.preventDefault();

    // Client side validation
    if (password !== password2) {
      setAlert('Passwords do not match', 'danger');
    } else {
      register({ name, email, password });
    }
  };

  if (isAuthenticated) {
    return <Redirect to='/dashboard' />;
  }

  return (
    <div
      className='d-flex flex-column align-items-center w-100 h-100'
      style={style}
    >
      <h3>Register</h3>
      <form
        className='p-3'
        style={{ width: '360px' }}
        onSubmit={(e) => onSubmit(e)}
      >
        <div className='form-group'>
          <input
            type='text'
            className='form-control'
            placeholder='Full name'
            name='name'
            value={name}
            onChange={(e) => onChange(e)}
            required
          />
        </div>

        <div className='form-group'>
          <input
            type='email'
            className='form-control'
            placeholder='Email'
            name='email'
            value={email}
            onChange={(e) => onChange(e)}
            required
          />
        </div>

        <div className='form-group'>
          <input
            type='password'
            className='form-control'
            placeholder='Password'
            name='password'
            value={password}
            onChange={(e) => onChange(e)}
            minLength='6'
          />
        </div>

        <div className='form-group'>
          <input
            type='password'
            className='form-control'
            placeholder='Confirm your password'
            name='password2'
            value={password2}
            onChange={(e) => onChange(e)}
            minLength='6'
          />
        </div>

        <button type='submit' className='btn btn-secondary w-100'>
          Submit
        </button>
      </form>
    </div>
  );
};

Register.propTypes = {
  register: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool,
  setAlert: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps, { register, setAlert })(Register);
