import { combineReducers } from 'redux';
import alerts from './alerts';
import auth from './auth';
import clients from './clients';
import reports from './reports';

export default combineReducers({ alerts, auth, clients, reports });
