import {
  ADD_REPORT,
  GET_REPORTS,
  DELETE_REPORT,
  REPORTS_ERROR,
  SHOW_PRELOADER,
  HIDE_PRELOADER,
} from '../actions/types';

const initialState = {
  reports: null,
  loading: true,
  generating: false,
};

export default (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case ADD_REPORT:
      return {
        ...state,
        reports: [payload, ...state.reports],
      };

    case GET_REPORTS:
      return {
        ...state,
        reports: payload,
        loading: false,
      };

      case DELETE_REPORT:
        return {
          ...state,
          reports: state.reports.filter(r => r.id != payload.id )
      };


    case REPORTS_ERROR:
      return {
        ...state,
        reports: null,
        loading: false,
        generating: false,
      };

    case SHOW_PRELOADER:
      return { ...state, generating: true };

    case HIDE_PRELOADER:
      return { ...state, generating: false };

    default:
      return state;
  }
};
