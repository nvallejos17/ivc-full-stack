import {
  GET_CLIENTS,
  CLIENTS_ERROR,
  ADD_CLIENT,
  EDIT_CLIENT,
  SELECT_CLIENT,
  FILTER_CLIENTS,
  CLEAR_FILTER,
  UPLOAD_LOGO,
} from '../actions/types';

const initialState = {
  clients: null,
  loading: true,
  selectedClient: null,
  filtered: null,
};

export default (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_CLIENTS:
      return {
        ...state,
        clients: payload,
        loading: false,
      };

    case CLIENTS_ERROR:
      return {
        ...state,
        clients: null,
        loading: false,
      };

    case ADD_CLIENT:
      return {
        ...state,
        clients: [payload, ...state.clients],
      };

    case EDIT_CLIENT:
      return {
        ...state,
        clients: state.clients.map((client) =>
          client._id === payload._id ? payload : client
        ),
        loading: false,
        selectedClient: payload,
      };

    case UPLOAD_LOGO:
    case SELECT_CLIENT:
      return {
        ...state,
        selectedClient: payload,
      };

    case FILTER_CLIENTS:
      return {
        ...state,
        filtered: state.clients.filter((client) => {
          const regex = new RegExp(`${payload}`, 'gi');
          return client.name.match(regex) || client.email.match(regex);
        }),
      };

    case CLEAR_FILTER:
      return {
        ...state,
        filtered: null,
      };

    default:
      return state;
  }
};
