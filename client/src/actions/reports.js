import {
  ADD_REPORT,
  GET_REPORTS,
  DELETE_REPORT,
  REPORTS_ERROR,
  SHOW_PRELOADER,
  HIDE_PRELOADER,
} from './types';

import axios from 'axios';

import { setAlert } from './alerts';

// Get (selectedClient) reports
export const getReports = (client) => async (dispatch) => {
  try {
    const res = await axios.get(`/api/reports/${client._id}`);

    dispatch({
      type: GET_REPORTS,
      payload: res.data, // Contains reports
    });

    console.log('Get reports success');
  } catch (err) {
    dispatch({
      type: REPORTS_ERROR,
    });

    // Show errors
    console.error('Get reports failed');
    // const errors = err.response.data.errors;
    // errors &&
    //   errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
  }
};

// Add report
export const addReport = (report) => async (dispatch) => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  try {
    const res = await axios.post('/api/reports', report, config);

    dispatch({
      type: ADD_REPORT,
      payload: res.data,
    });

    // Show alert
    dispatch(setAlert('Report added', 'success'));
  } catch (err) {
    // Show errors
    console.error('Add report failed');
    // const errors = err.response.data.errors;
    // errors &&
    //   errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
  }
};

// Delete report
export const deleteReport = (report) => async (dispatch) => {
  try {
    //eliminar de DB
    await axios.delete(`api/reports/${report.filename}`)

    // Actualizar estado de reportes
    dispatch({
      type: DELETE_REPORT,
      payload: report
    })
  } catch (error) {
      console.error('Delete report failed');
  }

}

// Show report preloader
export const showPreloader = () => (dispatch) => {
  dispatch({
    type: SHOW_PRELOADER,
  });
};

// Hide report preloader
export const hidePreloader = () => (dispatch) => {
  dispatch({
    type: HIDE_PRELOADER,
  });
};
