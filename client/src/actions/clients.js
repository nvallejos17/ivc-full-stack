import {
  GET_CLIENTS,
  CLIENTS_ERROR,
  ADD_CLIENT,
  EDIT_CLIENT,
  SELECT_CLIENT,
  FILTER_CLIENTS,
  CLEAR_FILTER,
  UPLOAD_LOGO,
} from './types';

import axios from 'axios';

import { setAlert } from './alerts';

// Upload logo (receives FormData with logoFile and clientId)
export const uploadLogo = (clientData) => async (dispatch) => {
  try {
    const res = await axios.put('/api/clients/upload', clientData);

    dispatch({
      type: UPLOAD_LOGO,
      payload: res.data, // Contains 'client' with logoUrl updated
    });

    // Show alert
    dispatch(setAlert('Logo uploaded', 'success'));
  } catch (err) {
    // Show errors
    const errors = err.response.data.errors;
    errors &&
      errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
  }
};

// Get clients
export const getClients = () => async (dispatch) => {
  try {
    const res = await axios.get('/api/clients');

    dispatch({
      type: GET_CLIENTS,
      payload: res.data, // Contains 'clients'
    });

    console.log('Get clients success');
  } catch (err) {
    dispatch({
      type: CLIENTS_ERROR,
    });

    // Show errors
    const errors = err.response.data.errors;
    errors &&
      errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
  }
};

// Add client
export const addClient = (client) => async (dispatch) => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  try {
    const res = await axios.post('/api/clients', client, config);

    dispatch({
      type: ADD_CLIENT,
      payload: res.data,
    });

    // Show alert
    dispatch(setAlert('Client added', 'success'));
  } catch (err) {
    // Show errors
    const errors = err.response.data.errors;
    errors &&
      errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
  }
};

// Edit client
export const editClient = (client) => async (dispatch) => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  try {
    const res = await axios.put(`/api/clients/${client._id}`, client, config);

    dispatch({
      type: EDIT_CLIENT,
      payload: res.data,
    });

    // Show alert
    dispatch(setAlert('Client updated', 'success'));
  } catch (err) {
    // Show errors
    const errors = err.response.data.errors;
    errors &&
      errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
  }
};

// Select client
export const selectClient = (client) => (dispatch) => {
  dispatch({
    type: SELECT_CLIENT,
    payload: client,
  });
};

// Filter contacts
export const filterClients = (text) => (dispatch) => {
  dispatch({
    type: FILTER_CLIENTS,
    payload: text,
  });
};

// Clear filter
export const clearFilter = () => (dispatch) => {
  dispatch({
    type: CLEAR_FILTER,
  });
};
