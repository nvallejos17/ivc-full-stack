// ALERTS
export const SET_ALERT = 'SET_ALERT';
export const REMOVE_ALERT = 'REMOVE_ALERT';

// AUTH
// export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
// export const REGISTER_FAIL = 'REGISTER_FAIL';
export const USER_LOADED = 'USER_LOADED';
export const AUTH_ERROR = 'AUTH_ERROR';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';
export const LOGOUT = 'LOGOUT';

// CLIENTS
export const GET_CLIENTS = 'GET_CLIENTS';
export const CLIENTS_ERROR = 'CLIENTS_ERROR';
export const ADD_CLIENT = 'ADD_CLIENT';
export const EDIT_CLIENT = 'EDIT_CLIENT';
export const SELECT_CLIENT = 'SELECT_CLIENT';
export const FILTER_CLIENTS = 'FILTER_CLIENTS';
export const CLEAR_FILTER = 'CLEAR_FILTER';
export const UPLOAD_LOGO = 'UPLOAD_LOGO';

// REPORTS
export const ADD_REPORT = 'ADD_REPORT';
export const GET_REPORTS = 'GET_REPORTS';
export const DELETE_REPORT = 'DELETE_REPORT';
export const REPORTS_ERROR = 'REPORTS_ERROR';
export const SHOW_PRELOADER = 'SHOW_PRELOADER';
export const HIDE_PRELOADER = 'HIDE_PRELOADER';
