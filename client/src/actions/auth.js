import {
  // REGISTER_SUCCESS,
  // REGISTER_FAIL,
  USER_LOADED,
  AUTH_ERROR,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
} from './types';

import axios from 'axios';
import setAuthToken from '../utils/setAuthToken';

import { setAlert } from './alerts';

// Load user
export const loadUser = () => async (dispatch) => {
  if (localStorage.token) {
    // Put token in header to make requests
    setAuthToken(localStorage.token);
  }

  try {
    const res = await axios.get('/api/users');

    dispatch({
      type: USER_LOADED,
      payload: res.data, // Contains 'user'
    });
  } catch (err) {
    dispatch({
      type: AUTH_ERROR,
    });

    // Show errors
    const errors = err.response.data.errors;
    errors &&
      errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
  }
};

// Register user
// export const register = ({ name, email, password }) => async (dispatch) => {
//   const config = {
//     headers: {
//       'Content-Type': 'application/json',
//     },
//   };

//   const body = JSON.stringify({ name, email, password });

//   try {
//     const res = await axios.post('/api/users/register', body, config);

//     dispatch({
//       type: REGISTER_SUCCESS,
//       payload: res.data, // Contains 'token'
//     });

//     dispatch(loadUser());
//   } catch (err) {
//     dispatch({
//       type: REGISTER_FAIL,
//     });

//     // Show errors
//     const errors = err.response.data.errors;
//     errors &&
//       errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
//   }
// };

// Login user
export const login = ({ email, password }) => async (dispatch) => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  const body = JSON.stringify({ email, password });

  try {
    const res = await axios.post('/api/users/login', body, config);

    dispatch({
      type: LOGIN_SUCCESS,
      payload: res.data, // Contains 'token'
    });

    dispatch(loadUser());
  } catch (err) {
    dispatch({
      type: LOGIN_FAIL,
    });

    // Show errors
    const errors = err.response.data.errors;
    errors &&
      errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
  }
};

// Logout
export const logout = () => (dispatch) => {
  dispatch({ type: LOGOUT });
  // window.location.reload(false);
};
