import React, { useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Navbar from './components/layout/Navbar';
import Landing from './components/layout/Landing';
import Dashboard from './components/users/Dashboard';
import Login from './components/auth/Login';
// import Register from './components/auth/Register';
import PrivateRoute from './components/routing/PrivateRoute';
import Alerts from './components/layout/Alerts';
import ReportPreloader from './components/reports/ReportPreloader';

import setAuthToken from './utils/setAuthToken';

// Redux
import { Provider } from 'react-redux';
import store from './store';

// Redux actions
import { loadUser } from './actions/auth';

// Check if there is a token in localStorage
if (localStorage.token) {
  // Put token in header to make requests
  setAuthToken(localStorage.token);
}

const App = () => {
  useEffect(() => {
    store.dispatch(loadUser());
  }, []);

  return (
    <Provider store={store}>
      <Router>
        <div className='main-container'>
          <Navbar />
          <Route exact path='/' component={Landing} />

          <ReportPreloader />
          <Alerts />
          <Switch>
            <PrivateRoute exact path='/dashboard' component={Dashboard} />
            <Route exact path='/login' component={Login} />
            {/* <Route exact path='/register' component={Register} /> */}
          </Switch>
        </div>
      </Router>
    </Provider>
  );
};

export default App;
