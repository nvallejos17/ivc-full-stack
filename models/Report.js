const mongoose = require('mongoose');

// Create schema
const ReportSchema = new mongoose.Schema({
  // Created by
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user',
  },

  // Belongs to
  clientId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'client',
    required: true,
  },

  // Required
  startDate: {
    type: Date,
    required: true,
  },
  endDate: {
    type: Date,
    required: true,
  },
  filename: {
    type: String,
  },

  // Issued
  date: {
    type: Date,
    default: Date.now,
  },
});

// Compile a model using the created schema
const Report = mongoose.model('report', ReportSchema);

module.exports = Report;
