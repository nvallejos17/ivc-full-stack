const mongoose = require('mongoose');

// Create schema
const ClientSchema = new mongoose.Schema({
  // Created by
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user',
  },

  // Required
  name: {
    type: String,
    required: true,
  },
  url: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  phone: {
    type: Number,
    required: true,
  },
  apiKey: {
    type: String,
    required: true,
  },

  // Misc
  editor: { type: String },
  address: { type: String },
  city: { type: String },
  classification: { type: String },
  subClassification: { type: String },
  logoUrl: { type: String },
  sections: {type: String},

  date: {
    type: Date,
    default: Date.now,
  },
});

// Compile a model using the created schema
const Client = mongoose.model('client', ClientSchema);

module.exports = Client;
