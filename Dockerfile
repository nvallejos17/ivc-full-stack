FROM node

WORKDIR /usr/src/app
COPY . /usr/src/app

# Install server dependencies
RUN npm install

# Install client dependecies
RUN npm install --prefix client

# Build client
RUN npm run build --prefix client

# Start server
CMD [ "node", "server" ]
